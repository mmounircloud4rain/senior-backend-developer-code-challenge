
**Cloud4Rain Senior Backend Developer Code Challenge**

The goal of this assignment is to show your coding skills and what you value in software engineering. We value new ideas so next to the original requirement feel free to improve/add/extend. We evaluate the assignment depending on your role (Developer/Tester) and your level of seniority.

**Challenge**

Create Node.js based backend application that provides API endpoints that can be used to GET/SET data from SQL/NOSQL based database (up to you), to different client side based applications (web/mobile).

 1. The client side application will allow the user to login/register.
 2. Logged in user can view and edit profile�s data.
 3. User can request reset password (forgot password process is up to you, surprise us!).
 4. Guest can register and should be activated in order to login (activation process is up to you, surprise us!).
 5. Logged in user can logout Different cases and user scenarios should be taken into consideration, we evaluate functionality, design for scalability and maintainability, and coverage.

**Requirements** 

 - Create and manage your application in a GIT-Repo from the start.
 
 - When committing, please provide a proper description.
 
 - Provide detailed description and documentation for your code.
 
 - State any external libraries if any.
